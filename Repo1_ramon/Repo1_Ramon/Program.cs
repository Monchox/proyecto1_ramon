﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;

namespace practica01_Ramon
{
    class Program
    {
        static void Main(string[] args)
        {

        }
        public static void ejercicio1()
        {
            //introducir 10 notas y decir aprobados y sobresalientes
            int aprobados, total, sobresaliente;
            aprobados = 0;
            total = 0;
            sobresaliente = 0;
            double[] nota = new double[10];
            while (total < 10)
            {
                Console.WriteLine("Introduzca la nota");
                nota[total] = double.Parse(Console.ReadLine());
                if (nota[total] >= 5)
                {
                    aprobados++;
                    if (nota[total] >= 9)
                    {
                        sobresaliente++;
                    }
                }
                total++;
            }
            Console.WriteLine("El numero de aprobados es " + aprobados + " y " + sobresaliente + " son sobresalientes.");
        }
        public static void ejercicio2()
        {
            //introducir fecha de ultima facturacion y devuelve la fecha de proxima facturacion teniendo en cuenta que todos los meses tienen 30 dias.
            string fecha1;
            string dia;
            string ano;
            string mes;
            int mes2;
            int ano2;
            Console.WriteLine("Introduzca ultima facturacion dd/mm/aaaa");
            fecha1 = Console.ReadLine();
            mes = fecha1.Substring(3, 2);
            dia = fecha1.Substring(0, 2);
            ano = fecha1.Substring(6, 4);
            mes2 = Int32.Parse(mes);
            ano2 = Int32.Parse(ano);
            mes2 = mes2 + 3;
            if (mes2 == 13)
            {
                mes2 = 1;
                ano2++;
            }
            if (mes2 == 14)
            {
                mes2 = 2;
                ano2++;
            }
            if (mes2 == 15)
            {
                mes2 = 3;
                ano2++;
            }
            Console.WriteLine("La proxima factura llega el dia " + dia + "/" + mes2 + "/" + ano2);

        }
        public static void ejercicio3()
        {
            //dar 10 numeros y el programa los leera al derecho (bucle for) y al reves (while)
            int[] num = new int[10];
            string dcha, izq;
            dcha = "";
            izq = "";
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Introducir numero");
                num[i] = Int32.Parse(Console.ReadLine());
            }
            for (int i = 0; i < 10; i++)
            {
                dcha = dcha + " " + num[i];
            }
            Console.WriteLine(dcha);
            int cont;
            cont = 9;
            while (cont >= 0)
            {
                izq = izq + " " + num[cont];
                cont = cont - 1;
            }
            Console.WriteLine(izq);
        }
        public static void ejercicio5()
        {
            //tomar 10 numeros e imprimir los dos mayores
            double[] num = new double[10];
            double max, max2;
            max2 = -1000;
            max = -1000;
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Introducir numero");
                num[i] = double.Parse(Console.ReadLine());
                max = Math.Max(max, num[i]);
            }
            for (int i = 0; i < 10; i++)
            {
                if (num[i] != max)
                {
                    max2 = Math.Max(max2, num[i]);
                }

            }
            Console.WriteLine("El mayor es " + max + " y el segundo es" + max2);
        }
        public static void ejercicio6()
        {
            int cont = 0;
            int contT = 1;
            int[] num = new int[10];
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("introducir numero");
                num[i] = Int32.Parse(Console.ReadLine());
            }
            for (int i = 0; i < 10; i++)
            {
                cont = 0;
                if (num[i] == num[0])
                {
                    cont++;
                }
                if (num[i] == num[1])
                {
                    cont++;
                }
                if (num[i] == num[2])
                {
                    cont++;
                }
                if (num[i] == num[3])
                {
                    cont++;
                }
                if (num[i] == num[4])
                {
                    cont++;
                }
                if (num[i] == num[5])
                {
                    cont++;
                }
                if (num[i] == num[6])
                {
                    cont++;
                }
                if (num[i] == num[7])
                {
                    cont++;
                }
                if (num[i] == num[8])
                {
                    cont++;
                }
                if (num[i] == num[9])
                {
                    cont++;
                }
                if (cont == 1)
                {
                    contT++;
                }
            }
            Console.WriteLine("Hay " + contT + " numeros diferentes.");
        }
    }
}